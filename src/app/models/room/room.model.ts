import { IRoom } from './room.interface';

export class Room {
  public id: string;
  public beds: number;
  public name: string;
  public rooms: string;

  constructor(register: IRoom) {
    this.id = (register && register.id) || null;
    this.beds = (register && register.beds) || null;
    this.name = (register && register.name) || null;
    this.rooms = (register && register.rooms) || null;
  }
}

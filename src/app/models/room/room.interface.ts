export interface IRoom {
  id: string;
  beds: number;
  name: string;
  rooms: string;
}

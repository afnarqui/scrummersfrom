export interface IReservation {
  id: string;
  availabilityId: string;
  rooms: string;
  checkin: string;
  checkout: string;
  name: string;
  beds: string;
}

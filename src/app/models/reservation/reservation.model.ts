import { IReservation } from './reservation.interface';

export class Reservation {
  public id: string;
  public availabilityId: string;
  public rooms: string;
  public checkin: string;
  public checkout: string;
  public name: string;
  public beds: string;

  constructor(register: IReservation) {
    this.id = (register && register.id) || null;
    this.availabilityId = (register && register.availabilityId) || null;
    this.rooms = (register && register.rooms) || null;
    this.checkin = (register && register.checkin) || null;
    this.checkout = (register && register.checkout) || null;
    this.name = (register && register.name) || null;
    this.beds = (register && register.beds) || null;
  }
}

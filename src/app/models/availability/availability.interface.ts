export interface IAvailability {
  id: string;
  dateinitial: string;
  dateend: string;
}

import { IAvailability } from './availability.interface';

export class Availability {
  public id: string;
  public dateinitial: string;
  public dateend: string;

  constructor(register: IAvailability) {
    this.id = (register && register.id) || null;
    this.dateinitial = (register && register.dateinitial) || null;
    this.dateend = (register && register.dateend) || null;
  }
}

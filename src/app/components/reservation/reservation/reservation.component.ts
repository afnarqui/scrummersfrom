import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IReservation } from 'src/app/models/reservation/reservation.interface';
import { MessageService } from 'src/app/service/message/message.service';
import { ReservationService } from 'src/app/service/reservation/reservation.service';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.css'],
})
export class ReservationComponent implements OnInit {
  @Input() valuesReservation: IReservation = {
    availabilityId: '',
    beds: '',
    checkin: '',
    checkout: '',
    id: '',
    name: '',
    rooms: '',
  };
  dateinitialUpdate = '';
  dateendUpdate = '';
  @Output() emits = new EventEmitter<boolean>();
  constructor(
    private serviceReservation: ReservationService,
    private serviceMessage: MessageService
  ) {}

  ngOnInit(): void {}

  /**
   *
   * @param items Updates an existing reservation
   */
  updateReservation(items: IReservation) {
    debugger;
    items.checkin = this.dateinitialUpdate;
    items.checkout = this.dateendUpdate;
    this.serviceReservation.update(items).subscribe(
      (data: any) => {
        // this.getReservation(); emitir evento
        this.serviceMessage.messageShow(
          'Reservation: ',
          data[0].data,
          'success'
        );
        this.emits.emit(true);
      },
      (error) => {
        console.log('er' + error);
      }
    );
  }

  changeStateReservation() {
    this.emits.emit(true);
  }
}

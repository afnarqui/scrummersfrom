import { Component, Input, OnInit } from '@angular/core';
import { IAvailability } from 'src/app/models/availability/availability.interface';
import { AvailabilityService } from 'src/app/service/availability/availability.service';
import { MessageService } from 'src/app/service/message/message.service';
import { ReservationService } from 'src/app/service/reservation/reservation.service';
import { createDebuggerStatement } from 'typescript';

@Component({
  selector: 'app-availability',
  templateUrl: './availability.component.html',
  styleUrls: ['./availability.component.css'],
})
export class AvailabilityComponent implements OnInit {
  @Input() valuesRoomsDate: any = [];
  availability: IAvailability = {
    id: '',
    dateinitial: '',
    dateend: '',
  };
  constructor(
    private service: AvailabilityService,
    private serviceMessage: MessageService,
    private serviceReservation: ReservationService
  ) {}

  ngOnInit(): void {
    this.get();
  }
  /**
   * Gets the availability for a room type for a set of days
   */
  get() {
    this.availability = {
      id: this.valuesRoomsDate[0].id,
      dateinitial: this.valuesRoomsDate[0].dateinitial,
      dateend: this.valuesRoomsDate[0].dateend,
    };

    this.service.get(this.availability).subscribe(
      (data: any) => {
        if (data[0].state === 1) {
          this.serviceMessage
            .messageConfirmation(
              'availability',
              '¿you want to reservation?',
              'warning',
              ['Cancel', 'Accept'],
              false
            )
            .then((valor) => {
              if (valor.value !== undefined) {
                console.log(this.availability);
                this.serviceReservation.post(this.availability).subscribe(
                  (data) => {
                    this.serviceMessage.messageShow(
                      'Reservation: ',
                      data[0].data,
                      'success'
                    );
                  },
                  (error) => {
                    this.serviceMessage.messageShow(
                      'Reservation: ',
                      data[0].data,
                      'error'
                    );
                  }
                );
              } else {
                console.log(this.availability);
              }
            });
        } else {
          this.serviceMessage.messageShow(
            'Rooms: ' + data[0].roomsId,
            data[0].name,
            'warning'
          );
        }
      },
      (error) => {
        console.log('er' + error);
      }
    );
  }
}

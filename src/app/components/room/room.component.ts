import { Component, OnInit } from '@angular/core';
import { IReservation } from 'src/app/models/reservation/reservation.interface';
import { IRoom } from 'src/app/models/room/room.interface';
import { MessageService } from 'src/app/service/message/message.service';
import { ReservationService } from 'src/app/service/reservation/reservation.service';
import { RoomService } from 'src/app/service/room/room.service';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css'],
})
export class RoomComponent implements OnInit {
  rooms: IRoom[] = [];
  searchAvailability: boolean = false;
  idRooms = '';
  reservation: IReservation[] = [];
  stateUpdateReservation = false;
  constructor(
    private service: RoomService,
    private serviceReservation: ReservationService,
    private serviceMessage: MessageService
  ) {}

  ngOnInit(): void {
    this.get();
    this.getReservation();
  }
  /**
   * Gets all rooms
   */
  get() {
    this.service.get().subscribe(
      (data: any) => {
        console.log(data);
        this.rooms = data;
      },
      (error) => {
        console.log('er' + error);
      }
    );
  }

  /**
   * Gets all reservation
   */
  getReservation() {
    this.serviceReservation.get().subscribe(
      (data: any) => {
        this.reservation = data;
      },
      (error) => {
        console.log('er' + error);
      }
    );
  }

  /**
   *
   * @param items Deletes an existing reservation
   */
  deleteReservation(items: IReservation) {
    this.serviceReservation.delete(items.id).subscribe(
      (data: any) => {
        this.getReservation();
        this.serviceMessage.messageShow(
          'Reservation: ',
          data[0].data,
          'success'
        );
      },
      (error) => {
        console.log('er' + error);
      }
    );
  }

  changeStateReservation() {
    this.stateUpdateReservation = !this.stateUpdateReservation;
  }

  emitEvent(event: boolean) {
    this.changeStateReservation();
    this.getReservation();
  }

  eventEmitModal(event: boolean) {
    this.getReservation();
  }
  /**
   * Open view for gets the availability for a room type for a set of days
   */
  async search(items: any) {
    this.idRooms = items.id;
    this.searchAvailability = true;
  }
}

import { Injectable } from '@angular/core';
import swal from 'sweetalert2';

@Injectable({
  providedIn: 'root',
})
export class MessageService {
  constructor() {}

  messageShow(title: any, text: any, icon: any) {
    swal.fire({
      title: title,
      text: text,
      icon: icon,
      background: '#fff url(/assets/images/logo.png) no-repeat',
      backdrop: `
      #9aa0a0
      url("/assets/images/logo.png")
      left top
      no-repeat`,
    });
  }

  messageConfirmation(
    title: any,
    text: any,
    icon: any,
    buttons: string[],
    dangerMode: any
  ) {
    return swal.fire({
      width: 600,
      padding: '3em',
      background: '#fff url(/assets/images/logo.png) no-repeat',
      backdrop: `
      #9aa0a0
      url("/assets/images/logo.png")
      left top
      no-repeat`,
      title,
      text,
      icon: icon,
      showCancelButton: true,
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
    });
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IAvailability } from 'src/app/models/availability/availability.interface';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AvailabilityService {
  url = environment.URL_SERVICE;

  constructor(private http: HttpClient) {}

  get(data: IAvailability) {
    return this.http.get(
      `${this.url}availability?id=${data.id}&dateInitial=${data.dateinitial}&dateEnd=${data.dateend}`
    );
  }
}

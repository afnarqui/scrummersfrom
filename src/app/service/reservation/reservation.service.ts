import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { IAvailability } from 'src/app/models/availability/availability.interface';
import { IReservation } from 'src/app/models/reservation/reservation.interface';

@Injectable({
  providedIn: 'root',
})
export class ReservationService {
  url = environment.URL_SERVICE;
  constructor(private http: HttpClient) {}
  /**
   *
   * @param items Makes a reservation for a room
   */
  post(availability: IAvailability) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post<IAvailability>(
      `${this.url}reservation?id=${availability.id}&dateInitial=${availability.dateinitial}&dateEnd=${availability.dateend}`,
      JSON.stringify(availability),
      { headers }
    );
  }

  /**
   * Gets all reservation
   */
  get() {
    return this.http.get(`${this.url}availability`);
  }

  /**
   *
   * @param id delete a reservation
   */
  delete(id: any) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.delete(`${this.url}reservation?id=${id}`, { headers });
  }

  /**
   *
   * @param items Updates an existing reservation
   */
  update(reservation: IReservation) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.put<IReservation>(
      `${this.url}reservation?id=${reservation.rooms}&reservationId=${reservation.id}&dateInitial=${reservation.checkin}&dateEnd=${reservation.checkout}`,
      JSON.stringify(reservation),
      { headers }
    );
  }
}

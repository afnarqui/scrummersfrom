import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class RoomService {
  url = environment.URL_SERVICE;

  constructor(private http: HttpClient) {}

  get() {
    return this.http.get(`${this.url}rooms`);
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoomComponent } from '../components/room/room.component';
import { HttpClientModule } from '@angular/common/http';
import { AvailabilityComponent } from '../components/availability/availability.component';
import { ModalComponent } from '../shared/modal/modal.component';
// ngBootstrap
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { ReservationComponent } from '../components/reservation/reservation/reservation.component';

@NgModule({
  declarations: [
    RoomComponent,
    AvailabilityComponent,
    ModalComponent,
    ReservationComponent,
  ],
  exports: [
    RoomComponent,
    AvailabilityComponent,
    ModalComponent,
    ReservationComponent,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    SweetAlert2Module.forChild(),
  ],
})
export class CoreModule {}

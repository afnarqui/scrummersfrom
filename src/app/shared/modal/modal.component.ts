import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewEncapsulation,
} from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./modal.component.css'],
})
export class ModalComponent implements OnInit {
  dateinitial: string;
  dateend: string;
  valuesInputs: Array<Object> = [];
  @Input() valuesRoomsId = '';
  @Output() emitModal = new EventEmitter<boolean>();
  constructor(public modal: NgbModal) {}

  ngOnInit(): void {}
  /**
   * Open modal with paramets
   */
  open(content, dateinitial, dateend) {
    this.valuesInputs = [];
    this.valuesInputs.push({
      dateinitial,
      dateend,
      id: this.valuesRoomsId,
    });
    this.modal.open(content, {
      scrollable: true,
      windowClass: 'modal_black',
      size: 'xl',
    });
  }

  close() {
    this.emitModal.emit(true);
    this.modal.dismissAll();
  }
}

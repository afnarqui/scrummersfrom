# scrummersFrom

[![N|Solid](https://firebasestorage.googleapis.com/v0/b/andresnaranjo-marcapersonal.appspot.com/o/prueba-sabado%2FCapture.JPG?alt=media&token=7f926f67-5c8f-46af-a3eb-14919271c65b)](https://nodesource.com/products/nsolid)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

project scrummersfrom. developed in angular using bootstrap

[trello](https://trello.com/b/GbenLBcC/scrummers) Trello

And of course scrummersfrom itself is open source with a [public repository][afn]
on GitLab.

## Usage

scrummersfrom requires

[Git](https://git-scm.com/downloads)

to run.

Install the dependencies and devDependencies previous
start the server.

## download

```sh
git clone https://gitlab.com/afnarqui/scrummersfrom
cd scrummersfrom
npm i
npm start
```

## run aplication in developement

```sh
http://localhost:4200/
```

## run aplication in production

```sh
http://138.121.170.105:8400/
```

[afn]: https://gitlab.com/afnarqui/scrummersfrom
